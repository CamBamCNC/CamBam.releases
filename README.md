# Welcome to the CamBam CNC development repository.

For documentation on using CamBam, please visit our [main website](http://CamBamCNC.com)

To discuss CamBam, seek advice or show off your projects, please visit our [user forum](http://www.cambam.co.uk/forum)

Thank you for your feedback and support. May your cutting tools be strong and your projects inspiring!

Andy [10Bulls]

## [Releases](/Releases)

Contains an archive of CamBam releases.

### [Latest stable Windows release...](/Releases/0.9/stable)
  
#### [CamBam 0.9.8P](https://gitlab.com/CamBamCNC/CamBam.releases/blob/master/Releases/0.9/stable/CamBamPlus-0.9.8P.msi)  
build 0.9.5729.19316, released 2015-09-22  
  
### [Latest  Windows development release...](Releases/1.0/alpha)
  
#### [CamBam 1.0 alpha-16](https://gitlab.com/CamBamCNC/CamBam.releases/blob/master/Releases/1.0/alpha/CamBamPlus-1.0-alpha16.msi)  
build 1.0.6752.163, released 2018-06-27      

### [Latest Linux development release...](Releases/1.0/alpha)

#### [CamBam 1.0 mono alpha-16](https://gitlab.com/CamBamCNC/CamBam.releases/blob/master/Releases/1.0/alpha/CamBam1.0-mono-alpha16-64bit.tgz)  
build 1.0.6753.24057, released 2018-06-28  

#### System folder  
[CamBam1.0-mono.system.tgz](https://gitlab.com/CamBamCNC/CamBam.releases/blob/master/Releases/1.0/alpha/CamBam1.0-mono.system.tgz)

### CamBam V1.0 offline documentation  
#### [CamBam1.0-documentation.zip](https://gitlab.com/CamBamCNC/CamBam.releases/blob/master/Releases/1.0/doc/CamBam1.0-documentation.zip)
